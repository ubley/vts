%bit-plane dekomposition

clear
clc

I=imread('cameraman.bmp');
I128=bitget(I,8);
I64=bitget(I,7);
I32=bitget(I,6);
I16=bitget(I,5);
I8=bitget(I,4);
I4=bitget(I,3);
I2=bitget(I,2);
I1=bitget(I,1);

I128_=128*I128;
I128_64=128*I128+64*I64;
I128_64_32=128*I128+64*I64+32*I32;
I128_64_32_16=128*I128+64*I64+32*I32+16*I16;
I128_64_32_16_8=128*I128+64*I64+32*I32+16*I16+8*I8;
I128_64_32_16_8_4=128*I128+64*I64+32*I32+16*I16+8*I8+4*I4;
I128_64_32_16_8_4_2=128*I128+64*I64+32*I32+16*I16+8*I8+4*I4+2*I2;
I128_64_32_16_8_4_2_1=128*I128+64*I64+32*I32+16*I16+8*I8+4*I4+2*I2+1*I1;

figure,
subplot(2,4,1), imshow(I128_,[0 256]), title('128')
subplot(2,4,2), imshow(I128_64,[0 256]), title('64')
subplot(2,4,3), imshow(I128_64_32,[0 256]), title('32')
subplot(2,4,4), imshow(I128_64_32_16,[0 256]), title('16')
subplot(2,4,5), imshow(I128_64_32_16_8,[0 256]), title('8')
subplot(2,4,6), imshow(I128_64_32_16_8_4,[0 256]), title('4')
subplot(2,4,7), imshow(I128_64_32_16_8_4_2,[0 256]), title('2')
subplot(2,4,8), imshow(I128_64_32_16_8_4_2_1,[0 256]), title('1')

%subplot(2,4,1), imshow(I128,[0 1]), title('128')
%subplot(2,4,2), imshow(I64,[0 1]), title('64')
%subplot(2,4,3), imshow(I32,[0 1]), title('32')
%subplot(2,4,4), imshow(I16,[0 1]), title('16')
%subplot(2,4,5), imshow(I8,[0 1]), title('8')
%subplot(2,4,6), imshow(I4,[0 1]), title('4')
%subplot(2,4,7), imshow(I2,[0 1]), title('2')
%subplot(2,4,8), imshow(I1,[0 1]), title('1')

