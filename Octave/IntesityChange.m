%ket intenzitas transzformacio jellemgorbe

clear
clc

I = imread ('lena.bmp');


I1 = zeros(256,256);
I2 = zeros(256,256);

for i = 1 : 256
  for j = 1 : 256
     if (I(i,j) > 100 && I(i,j) < 200)
       I1(i,j) = I(i,j)+20;
       I2(i,j) = 230;
     end
  end
end



subplot(1,3,1), imshow(I, [0 255]), title('Original Image')
subplot(1,3,2), imshow(I1, [0 255]), title('Original+20')
subplot(1,3,3), imshow(I2, [0 255]), title('100-200:230')