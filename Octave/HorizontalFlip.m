% horizonzal flip
clear
clc

I = imread ('cameraman.bmp');
Iflip = zeros(256);

for i = 1 : 256
  for j = 1 : 256
    Iflip(i,j) = I(i, 257-j);
  endfor
endfor

subplot(1,2,1), imshow(I,[0 255])
subplot(1,2,2), imshow(Iflip,[0 255])
