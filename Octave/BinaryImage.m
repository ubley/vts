

pkg load image
clear
clc

I = imread('lena.bmp');

I128 = zeros(256);
I200 = zeros(256);
I50 = zeros(256);

for i = 1 : 256
  for j = 1 : 256
    if I(i,j) > 127
      I128(i,j) = 255;
    else
      I128(i,j) = 0;
    end
  end
end

for i = 1 : 256
  for j = 1 : 256
    if I(i,j) > 200
      I200(i,j) = 255;
    else
      I200(i,j) = 0;
    end
  end
end

for i = 1 : 256
  for j = 1 : 256
    if I(i,j) > 50
      I50(i,j) = 255;
    else
      I50(i,j) = 0;
    end
  end
end

subplot(1,4,1), imshow(I, [0,255]), title('Original')
subplot(1,4,2), imshow(I128, [0,255]), title('Treshold=128')
subplot(1,4,3), imshow(I200, [0,255]), title('Treshold=200')
subplot(1,4,4), imshow(I50, [0,255]), title('Treshold=50')
