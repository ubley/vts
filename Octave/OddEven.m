

pkg load image

clear
clc

I = imread('lena.bmp');

Iodd = zeros(256);
Ieven = zeros(256);


for i = 1 : 256
  for j = 1 : 256
    if rem(I(i,j),2) == 0
      Iodd(i,j) = 0;
      Ieven(i,j) = I(i,j);
    else
      Iodd(i,j) = I(i,j);
      Ieven(i,j) = 0;
    endif
  end
end

subplot(1,3,1), imshow(I, [0,255]), title('Original')
subplot(1,3,2), imshow(Ieven, [0,255]), title('even')
subplot(1,3,3), imshow(Iodd, [0,255]), title('odd')
