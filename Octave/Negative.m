%negative

clear
clc

I = imread ('cameraman.bmp');


Ineg = zeros(256,256);

for i = 1 : 256
  for j = 1 : 256
    Ineg(i,j) = 255-I(i,j);
    end
end

subplot(1,2,1), imshow(I, [0 255]), title('Original Image')
subplot(1,2,2), imshow(Ineg, [0 255]), title('negative image')