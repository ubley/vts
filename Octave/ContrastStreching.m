% contrast streching example

clear
clc

I = imread ('pout.bmp');
I = double(I);
Imax = max(max(I));
Imin = min(min(I));

Ihigh = zeros(291,240);

for i = 1 : 291
  for j = 1 : 240
    Ihigh(i,j) = ((I(i,j)-Imin)/(Imax-Imin))*(2^8-1);
    end
end

subplot(2,2,1), imshow(I, [0 255]), title('Original Image')
subplot(2,2,3), imhist(I/256), title('Original Image histogram')
subplot(2,2,2), imshow(Ihigh, [0 255]), title('Contrast streched image')
subplot(2,2,4), imhist(Ihigh/256), title('Contrast streched Image histogram')