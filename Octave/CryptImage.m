%CryptImage
clear
clc
pkg load image
I = imread('cameraman.bmp');
secretKey = randperm(65536);
Icrypt = zeros(256);
Icrypt = uint8(Icrypt);
for i = 1 : 65536
  Icrypt(i) = I(secretKey(i));
endfor
subplot(2,2,1), imshow(I, [0 255]), title('Original image')
subplot(2,2,2), imshow(Icrypt, [0 255]), title('Crypted image')
subplot(2,2,3), imhist(I), axis([0 255 0 1800])
subplot(2,2,4), imhist(Icrypt), axis([0 255 0 1800])
