%border

clc
clear

I1=imread('lena.bmp');
I2=zeros(264);


%for i = 1 : 264
%  for j = 1 : 264
%      I2(i,j) = 255;
%  endfor
%endfor

%I2=(5:260,5:260)=I
for i = 5 : 260
  for j = 5 : 260
      I2(i,j) = I1(i-4,j-4);
  endfor
endfor

I2(1:6,:)=170;
I2(259:264, :)=170;
I2(:, 1:6)=170;
I2(:, 259:264)=170;

I2=uint8(I2);
subplot(1,2,1), imshow(I1, [0 255]), title('Original')
subplot(1,2,2), imshow(I2, [0 255]), title('Framed Image')
