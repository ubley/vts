use SzinhPortal;

select * from dbo.ELOADAS_PROJEKT

--Listazzuk a 140percnel hosszabb eloadas cimeket


select ELOADAS_CIME, EL_HOSSZA_PERC
	from dbo.ELOADAS_PROJEKT
	where EL_HOSSZA_PERC > 140;
	


--Listazzuk a 140percnel hosszabb eloadas cimeket, amelyeknek
--2014-ben volt a bemutatoja


select ELOADAS_CIME, EL_HOSSZA_PERC, YEAR(EL_BEMUTATO_DATUM)
	from dbo.ELOADAS_PROJEKT
	where EL_HOSSZA_PERC > 140 and
		YEAR(EL_BEMUTATO_DATUM)=2014;
	
	
select * from dbo.ELOADAS_PROJEKT



--Listazzuk a 139percnel hosszabb eloadas cimeket, amelyeknek
--decemberben volt a bemutatoja


select ELOADAS_CIME, EL_HOSSZA_PERC, month(EL_BEMUTATO_DATUM)
	from dbo.ELOADAS_PROJEKT
	where EL_HOSSZA_PERC >= 140 and
		month(EL_BEMUTATO_DATUM)=12;
		
		

--Listazzuk a 140percnel hosszabb eloadas cimeket, amelyeknek
--a honap harmadik napjan volt a bemutatoja


select ELOADAS_CIME, EL_HOSSZA_PERC, day(EL_BEMUTATO_DATUM)
	from dbo.ELOADAS_PROJEKT
	where EL_HOSSZA_PERC > 140 and
		day(EL_BEMUTATO_DATUM)=3;
		
		
		

--Listazzuk a 140percnel hosszabb eloadas cimeket, amelyeknek
--a felvonas szama nagyobb mint 1


select ELOADAS_CIME, EL_HOSSZA_PERC, EL_FELVONAS_SZAMA
	from dbo.ELOADAS_PROJEKT
	where EL_HOSSZA_PERC > 140 and
		EL_FELVONAS_SZAMA>1;
		
		
				
		

--Listazzuk a 140percnel hosszabb eloadas cimeket, amelyeknek
--a felvonas szama nagyobb mint 1 es ot evvel ezelotti evben volt a bemutatojuk


select ELOADAS_CIME, EL_HOSSZA_PERC, EL_FELVONAS_SZAMA
	from dbo.ELOADAS_PROJEKT
	where EL_HOSSZA_PERC > 140 and
		EL_FELVONAS_SZAMA>1 and
		YEAR(EL_BEMUTATO_DATUM)=YEAR(getdate())-5;
		
		
		

--Listazzuk a budapesti es ujvideki szinhazakat


select SZINHAZNEV, SZ_H_CIM_HELYSEG
	from dbo.SZINHAZ
	where SZ_H_CIM_HELYSEG = 'Budapest' or
	SZ_H_CIM_HELYSEG = 'Újvidék';

--VAGY

select SZINHAZNEV, SZ_H_CIM_HELYSEG
	from dbo.SZINHAZ
	where SZ_H_CIM_HELYSEG in ('Budapest','Újvidék');
	
--


--Listazzuk a budapesti es ujvideki szinhazakat

	
	
select SZINHAZNEV, SZ_H_CIM_HELYSEG
	from dbo.SZINHAZ
	where HELYISEG_AZ in (select HELYISEG_AZ
							from dbo.HELYISEG
							where HELYISEGNEV in ('Budapest','Újvidék'));
	
	

--Listazzuk azokat a szindarabokiat, amelyeknek nincs forditojuk


select SZINDARAB_CIME
	from SZINDARAB
	where SZEMELY_AZ_FORD is null;
	

	
	
	
