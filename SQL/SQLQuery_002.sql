use arurendeles;

select * from dbo.vevo;

insert dbo.vevo
	values(4, 'Vevo1', 24207, 'Orom', 'Sotet utca');
	
alter table vevo
	drop constraint uc_IrszamSzabadkaiKorzet;

	
alter table vevo
	add constraint uc_IrszamSzabadkaiKorzet
		check (ir_szam between 24000 and 24999);
		
insert dbo.vevo (Vevo_kod)
	values(4);
	
update Vevo
	set Utca_hsz='M.O 16.';
	
update Vevo
	set Utca_hsz='Marka Oreskovica 16.'
	where Vevo_nev='Vevo1';


update Vevo
	set
		Utca_hsz='Marka Oreskovica 18.',
		Helyseg='Kanizsa'
	where Vevo_kod=2 or Vevo_kod=3;

delete dbo.vevo
	where Vevo_kod=3;
	
	
create view vevo_nezet_1
	as select vevo_nev, helyseg from vevo;
	
select * from vevo_nezet_2;


create view vevo_nezet_2 ("Vevonev_nezet", "Helysegnev_nezet")
	as select vevo_nev, helyseg from vevo;
	


select * from dbo.vevo;
	