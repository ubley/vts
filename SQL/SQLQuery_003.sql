create database Hallgatoeredmenyek;
use Hallgatoeredmenyek;

create table Hallgato
(
Indexszam char(6) not null,
Vezeteknev varchar(15) not null,
Nev varchar(15) not null,
SzulDatum date not null,
constraint HallgAzon primary key (Indexszam)
);

create table TanulmanyiProgram
(
TanulmProgAzon tinyint not null,
TanulmProgNev varchar(50) not null,
TanulmProgBevezEve numeric(4,0) not null,
TanulmProgErvVege decimal(4,0) not null,
constraint TPAzon primary key (TanulmProgAzon)
);

create table Vizsgaidoszak
(
Iskolaev char(5) not null,
Vizsgaidoszak tinyint not null,
VizsIdKezdete date not null,
VizsIdVege date not null
constraint VizsgIdAzon primary key(Iskolaev, Vizsgaidoszak)
);

create table Targy
(
TanulmProgAzon tinyint not null foreign key references TanulmanyiProgram(TanulmProgAzon),
TargyAzon int not null,
TargyNev varchar(30) not null,
Felev tinyint not null,
HetiEloadasSzam tinyint not null,
HetiLaborSzam tinyint null,
constraint TAzon primary key (TanulmProgAzon, TargyAzon)
);

create table Eredmeny
(
TanulmProgAzon tinyint not null foreign key references Targy(TanulmProgAzon),
TargyAzon int not null foreign key references Targy(TargyAzon),
Indexszam char(6) not null foreign key references Hallgato(Indexszam),
Iskolaev char(5) not null foreign key references Vizsgaidoszak(Iskolaev),
Vizsgaidoszak tinyint not null foreign key references Vizsgaidoszak(Vizsgaidoszak),
Jegy numeric(2,0) not null,
JegyBeirDatum date not null,
constraint EredmAzon primary key (TanulmProgAzon, TargyAzon, Indexszam, Iskolaev, Vizsgaidoszak)
);
