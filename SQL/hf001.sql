create database arurendeles;

use arurendeles;

create table vevo (
Vevo_kod int,
Vevo_nev varchar(30) not null,
constraint Vevo_azon primary key(Vevo_kod),
ir_szam numeric(5,0) default 24000  check(ir_szam>=20000 and ir_szam<29999),
Helyseg varchar(20),
Utca_hsz varchar(20));

create table Aru_arlista (
Aru_kod int,
Mertekegyseg char(10),
Aru_megnevezes varchar(20) not null,
Egysegar numeric(15,2) not null check (Egysegar>0),
constraint Aru_azon primary key(Aru_kod, Mertekegyseg)
);

create table Rendeles (
Rendeles_sz int,
Vevo_kod int,
Kelt Date not null default getdate(),
constraint Rendeles_azon primary key(Rendeles_sz),
foreign key(Vevo_kod) references vevo(Vevo_kod) on delete cascade on update cascade
);

create table Rendeles_tetel (
Aru_kod int,
Mertekegyseg char(10),
Rendeles_sz int,
Rendelt_menny numeric(12,2) not null check (Rendelt_menny>0),
constraint Identifier_1 primary key(Aru_kod, Mertekegyseg, Rendeles_sz),
foreign key(Aru_kod, Mertekegyseg) references aru_arlista(Aru_kod, Mertekegyseg) on delete no action on update cascade,
foreign key(Rendeles_sz) references Rendeles(Rendeles_sz)
);

drop table Aru_arlista;
drop table Rendeles_tetel;
drop table vevo;
drop table Rendeles;



