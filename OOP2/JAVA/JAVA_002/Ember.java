
public class Ember {
	private String nev;
	private int szul_datum;
	private boolean neme;
	private final boolean FERFI=true;
	private final boolean NO=true;
	
	public Ember() {
		nev="Nevenincs";
		szul_datum=1900;
		neme=FERFI;
	}
	public Ember(String nev0, int datum, boolean neme0) {
		this.nev=nev0;
		this.szul_datum=datum;
		this.neme=neme0;
	}
	public void setNev(String nev0) {
		this.nev=nev0;
	}
	public void setDatum(int datum) {
		this.szul_datum=datum;
	}
	public String getNev() {
		return nev;
	}
	public int getDatum() {
		return szul_datum;
	}
	public boolean getName() {
		return neme;
	}
	public void print1() {
		System.out.println("Az ember neve " + nev + " aki szuletett " + szul_datum + " es mellesleg " + (neme?"ferfi":"no"));
	}
	public static void print2(String nev, int szul_datum, boolean neme) {
		System.out.println("Az ember neve " + nev + " aki szuletett " + szul_datum + " es mellesleg " + (neme?"ferfi":"no"));
	}
	public String toString() {
		return "Az ember neve " + nev + " aki szuletett " + szul_datum + " es mellesleg " + (neme?"ferfi":"no");
	}
}
