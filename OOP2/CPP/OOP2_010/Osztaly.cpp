#include <iostream>
#include "Osztaly.h"
using namespace std;

	Szemely::Szemely (string nev0, int szul_ev0, string szul_hely0)
	{
		nev=nev0;
		szul_ev=szul_ev0;
		szul_hely=szul_hely0;
	}

	void Szemely::adat_kiir()
	{
		cout << "\nSzemely neve = " 	<< nev <<
				"\nSzuletett = " 		<< szul_ev <<
				"\nSzuletesi hely = " 	<< szul_hely;
	}

	void Diak::adat_kiir()
	{
		Szemely::adat_kiir();
		cout << "\nEref kod = " 		<< eref_kod <<
				"\nEv folyam = "		<< evfolyam;
	}

	void Tanar::adat_kiir()
	{
		Szemely::adat_kiir();
		cout << "\nSzak = " 		<< szak;
	}