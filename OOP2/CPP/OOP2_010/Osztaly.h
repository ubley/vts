#ifndef OSZTALY_H_INCLUDED
#define OSZTALY_H_INCLUDED
#include <string>
using namespace std;

class Szemely
{
	string nev;
	int szul_ev;
	string szul_hely;
public:
	Szemely (string nev0, int szul_ev0, string szul_hely0);
	void adat_kiir();
};

class Diak:public Szemely
{
	int eref_kod;
	int evfolyam;
public:
	Diak (string nev0, int szul_ev0, string szul_hely0, int eref_kod0, int evfolyam0)
		:Szemely(nev0, szul_ev0, szul_hely0)
	{
		eref_kod=eref_kod0;
		evfolyam=evfolyam0;
	}
	void adat_kiir();
};

class Tanar:public Szemely
{
	string szak;
public:
	Tanar(string nev0, int szul_ev0, string szul_hely0, string szak0) : Szemely(nev0, szul_ev0, szul_hely0)
	{
		szak=szak0;
	}
	void adat_kiir();
};


#endif // OSZTALY_H_INCLUDED
