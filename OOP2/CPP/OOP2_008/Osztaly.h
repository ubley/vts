#ifndef OSZTALY_H_INCLUDED
#define OSZTALY_H_INCLUDED

class Vehicle
{

	double weight;
	int wheels;
public:
	Vehicle(); //default constructor
	Vehicle(double, int); //parameteres constructor
	void init(double, int);
	double get_weight();
	int get_wheels();
	~Vehicle();
};

class Car : public Vehicle
{
	int passengers;
public:
	Car();
	Car(double, int, int);
	void init(double, int, int);
	int get_passengers();
	~Car();
};

class Truck : public Vehicle
{
protected:			//Tovabbi oroklodesnel lesz jelentosege
	int passengers;
	double pay_load; //teherbiras
public:
	Truck();
	Truck(double, int, int, double);
	void init(double, int, int, double);
	int get_passengers();
	double efficiency(); //(payload+weight)/wheels
	~Truck();
};

class Bus : public Car
{
	bool emeletes;
	double csomagter_nagysaga;
public:
	Bus();
	Bus(double, int, int, bool, double);
	bool get_emelet();
	double get_csomagter_nagysaga();
	~Bus();
};

#endif // OSZTALY_H_INCLUDED
