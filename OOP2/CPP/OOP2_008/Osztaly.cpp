#include "Osztaly.h"
#include <iostream>

using namespace std;

//VEHICLE//
void Vehicle::init(double weight0, int wheels0)
{
	weight=weight0;
	wheels=wheels0;
}

Vehicle::Vehicle()
{
	weight=0;
	wheels=0;
}

Vehicle::Vehicle(double weight0, int wheels0)
{
	weight=weight0;
	wheels=wheels0;
}

double Vehicle::get_weight()
{
	return weight;
}

int Vehicle::get_wheels()
{
	return wheels;
}

Vehicle::~Vehicle()
{
	cout << "Vehicle destruktora!" << endl;
}

//CAR//
void Car::init(double weight0, int wheels0, int passengers0)
{
	//weight=weight0;   //protectednel dolgozik
	Vehicle::init(weight0,wheels0);
	//wheels=wheels0;   //protectednel dolgozik
	passengers=passengers0;
}

Car::Car():Vehicle()
{
	passengers=0;

}

Car::Car(double weight0, int wheels0, int passengers0):Vehicle(weight0, wheels0)
{
	passengers=passengers0;
}

int Car::get_passengers()
{
	return passengers;
}

Car::~Car()
{
	cout << "Car destruktora!" << endl;
}

//TRUCK//
void Truck::init(double weight0, int wheels0, int passengers0, double pay_load0)
{
	//weight=weight0;
	//wheels=wheels0;
	Vehicle::init(weight0, wheels0);
	passengers=passengers0;
	pay_load=pay_load0;

}

Truck::Truck():Vehicle()
{
	passengers=0;
	pay_load=0;
}

Truck::Truck(double weight0, int wheels0, int passengers0, double pay_load0):Vehicle(weight0, wheels0)
{
	passengers=passengers0;
	pay_load=pay_load0;
}

int Truck::get_passengers()
{
	return passengers;
}

double Truck::efficiency()
{
	return (pay_load+get_weight())/get_wheels();
}

Truck::~Truck()
{
	cout << "Truck destruktora!" << endl;
}

//BUS//
Bus::Bus():Car()
{
	emeletes=false;
	csomagter_nagysaga=0;
}

Bus::Bus(double weight0, int wheels0, int passengers0, bool emeletes0, double csomagter_nagysaga0):Car(weight0, wheels0, passengers0)
{
	emeletes=emeletes0;
	csomagter_nagysaga=csomagter_nagysaga0;
}

bool Bus::get_emelet()
{
	return emeletes;
}

double Bus::get_csomagter_nagysaga()
{
	return csomagter_nagysaga;
}

Bus::~Bus()
{
	cout << "Bus destruktora!" << endl;
}











