#include <iostream>
#include "Osztaly.h"

using namespace std;

int main()
{
	Vehicle yugo45(850.0, 4);
	//yugo45.init(850.0, 4);
	Car yugo55(850.0, 4, 4);
	//yugo55.init(880.0, 4, 4);
	Truck tam(3500.0, 6, 5, 300.0);
	//tam.init(3500.0,6 , 5, 300.0);
	Bus sutrans(3000.0, 6, 20, true, 3000.0);

	cout	<< "\nYugo45 adatai:" << endl
			<< "tomege= " << yugo45.get_weight() << endl
			<< "kerekek szama= " << yugo45.get_wheels() << endl;

	cout	<< "\nYugo55 adatai:" << endl
			<< "tomege= " << yugo55.get_weight() << endl
			<< "kerekek szama= " << yugo55.get_wheels() << endl
			<< "utasok szama= " << yugo55.get_passengers() << endl;

	cout	<< "\nKamion adatai:" << endl
			<< "tomege= " << tam.get_weight() << endl
			<< "kerekek szama= " << tam.get_wheels() << endl
			<< "utasok szama= " << tam.get_passengers() << endl
			<< "hatekonysag= " << tam.efficiency() << endl;

	cout	<< "\nBusz adatai:" << endl
			<< "tomege= " << sutrans.get_weight() << endl
			<< "kerekek szama= " << sutrans.get_wheels() << endl
			<< "utasok szama= " << sutrans.get_passengers() << endl
			<< "emeletes= " << sutrans.get_emelet() << endl
			<< "csomagter nagysaga= " << sutrans.get_csomagter_nagysaga() << endl;

	return 0;
}
