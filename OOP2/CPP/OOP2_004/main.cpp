#include <iostream>
#include "Osztaly.h"

using namespace std;

int main()
{
    Henger h1; //default contructorral inicializalva
    cout << "Henger terfogata = " <<h1.Terfogat()<<endl;

    Henger h2(10,20); //default contructorral inicializalva
    cout << "Henger terfogata = " <<h2.Terfogat()<<endl;

    Henger *ptr;
    ptr = new Henger();
    ptr->Init(10,5);
    cout << "Henger palast = " <<ptr->palast_terulete()<<endl;
    return 0;
}
