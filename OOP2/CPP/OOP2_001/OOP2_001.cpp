//2019.10.02
#include <iostream>
using namespace std;

class Calc
{
	float x;
	float y;
	char op;
public:
	float getX() {return x;}
	float getY() {return y;}
	char getOp() {return op;}

	void setX(float x0) {x = x0;}
	void setY(float y0) {y = y0;}
	void setOp(char op0) {op = op0;}

	void init (float x0, float y0, char op0);
	float szamol();
};


int main()
{
	Calc calc1;
	//Calc calc2;
	float x, y;
	char op;
	cin >> x >> op >> y;

	calc1.setX(x);
	calc1.setY(y);
	calc1.setOp(op);

	cout << calc1.getX() << calc1.getOp() << calc1.getY() << " = " << calc1.szamol() << endl;

	//calc1.init(3,5,'*');
	//calc2.init(3,5,'+');

	//cout << "x=" << calc1.getX() << endl;
	//cout << "y=" << calc1.getY() << endl;

	//calc1.szamol();
	//calc2.szamol();

	return 0;
}

void Calc::init(float x0, float y0, char op0)
{
	x = x0;
	y = y0;
	op = op0;
}
float Calc::szamol()
{
	float res;
	if (op == '+') res = x + y;
	if (op == '-') res = x - y;
	if (op == '*') res = x * y;
	if (op == '/') res = x / y;
	return res;
}
