#ifndef OSZTALY_H_INCLUDED
#define OSZTALY_H_INCLUDED
class Calc {
	float x;
	float y;
	char op;
 public:
	 Calc ();
	 Calc (float, float, char);
	 float getX(){ return x;};
	 float getY(){ return y;};
	 char getOp(){ return op;};
	 void setX(float x0){ x=x0; };
	 void setY(float y0){ y=y0; };
	 void setOp(char ch0){ op=ch0; };
	 void init(float x0, float y0, char op0);
	 float szamol();
 };
#endif // OSZTALY_H_INCLUDED
