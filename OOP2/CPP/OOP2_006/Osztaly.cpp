#include <iostream>
#include "Osztaly.h"
using namespace std;

    bool Set::is_equal(Set h0)
    {
        if(card != h0.card)
            return false;
        for(int i=0; i<card; i++)
            if(!h0.is_member(elems[i]))
            return false;
        return true;
    }

    void Set::add_member(int x)
    {
        if ( !is_member(x) )
        {
            if (card < 100)
                elems[card++]=x;
            else
                cout << "\nSet is full.";
        }
    };

    void Set::remove_member(int x)
    {
        for(int i=0; i<card; i++)
        {
            if (elems[i]==x)
            {
                for(int j=i; j<card-1; j++)
                    elems[j]=elems[j+1];
                card--;
            }
        }
    };

    bool Set::is_member(int x)
    {
        for(int i=0; i<card; i++)
            if (elems[i]==x)
                return true;
        return false;
    };

    void Set::print()
    {
        for(int i=0; i<card; i++)
            cout << elems[i] << " " << endl;
    }


