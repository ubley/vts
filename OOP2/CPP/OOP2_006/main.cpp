#include <iostream>
#include "Osztaly.h"

using namespace std;

int main()
{
    int n;
    Set h1, h2;                 // h1 objektum egy halmaz
    h1.empty_set();             //�res halmaz a h1
    for(int i=0; i<5; i++)
    {
        cout << "Kovetkezo elem: ";
        cin >> n;
        h1.add_member(n);
    }
    h1.print();
    h2.empty_set();             //�res halmaz a h2
    for(int i=0; i<5; i++)
    {
        cout << "Kovetkezo elem: ";
        cin >> n;
        h2.add_member(n);
    }
    h2.print();
    /*cout << "Torlendo elem: ";
    cin >> n;
    h1.remove_member(n);
    h1.print();*/
    if(h2.is_equal(h1))
        cout << "\nA ket halmaz egyenlo." << endl;
    else
        cout << "\nA ket halmaz NEM egyenlo." << endl;
    return 0;
}
