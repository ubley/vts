#ifndef OSZTALY_H_INCLUDED
#define OSZTALY_H_INCLUDED

class Set
{
private:
    int elems[100]; //elemek szama
    int card;       //elemek szama a halmazban

public:
    void empty_set(){card=0;}
    void add_member(int);
    void remove_member(int);
    bool is_member(int);
    void print();
    bool is_equal(Set);


};


#endif // OSZTALY_H_INCLUDED
