#include "Osztaly.h"
#include <iostream>
#include <math.h>
using namespace std;

////////////Teglalap_Teglatest/////////////////
//Teglalap
Teglalap::Teglalap(double a1, double a2)
{
	a_oldal = a1;
	b_oldal = a2;
};
void Teglalap::Szamol1()
{
	ered1 = a_oldal * 2 + b_oldal * 2;
};
void Teglalap::Szamol2()
{
	ered2 = a_oldal * b_oldal;
};
//Teglatest
Teglatest::Teglatest(double a1, double b1, double c1) : Teglalap(a1, b1)
{

	c_oldal = c1;
};
void Teglatest::Szamol1()
{
	ered1 = 2 * (a_oldal * b_oldal + a_oldal * c_oldal + b_oldal * c_oldal);
};
void Teglatest::Szamol2()
{
	ered2 = a_oldal * b_oldal * c_oldal;
};

//////////Kocka_negyzet////////
//negyzet
negyzet::negyzet(double a1)
{
	a_oldal = a1;
}
void negyzet::Szamol1()
{
	ered1 = 4 * a_oldal;
}
void negyzet::Szamol2()
{
	ered2 = a_oldal * a_oldal;
}
double AtlotSzamol(negyzet &n)
{
	n.atlo = n.a_oldal * sqrt(2);
	return n.atlo;
}
//kocka
kocka::kocka(double a1) : negyzet(a1)
{
}
void kocka::Szamol1()
{
	ered1 = 6 * a_oldal * a_oldal;
}
void kocka::Szamol2()
{
	ered2 = a_oldal * a_oldal * a_oldal;
}
double TestatlotSzamol(kocka &k)
{
	k.testatlo = k.a_oldal * sqrt(3);
	return k.testatlo;
}

/////////Kor_Gomb_Henger///////////////
//Kor
Kor::Kor(double r1)
{
	r = r1;
}
void Kor::Szamol1() //a kor kerulete
{
	ered1 = 2 * r * 3.14;
}
void Kor::Szamol2() //a kor terulete
{
	ered2 = r * r * 3.14;
}
//Gomb
Gomb::Gomb(double r1) : Kor(r1)
{
}
void Gomb::Szamol1() //a gomb felszine
{
	ered1 = 4 * r * r * 3.14;
}
void Gomb::Szamol2() //a gomb terfogata
{
	ered2 = (4 * r * r * r * 3.14) / 3;
}
//Henger
Henger::Henger(double r1, double h1) : Kor(r1)
{
	h = h1;
}
void Henger::Szamol1() //a henger felszine
{
	ered1 = 2 * r * 3.14 + 2 * 3.14 * h;
}
void Henger::Szamol2() //a henger terfogata
{
	ered2 = r * r * 3.14 * h;
}

///////DerekSzH_AltalanosH////////
//DerekSzH
void DerekSzH::Szamol1() //Derekszogu haromszog terulete
{
	ter = (a * b) / 2;
}
void DerekSzH::Szamol2() //Derekszogu haromszog kerulete
{
	ker = sqrt((a * a) + (b * b)) + a + b;
}
//AltalanosH
AltalanosH::AltalanosH(double a1, double b1, double gamma1) : DerekSzH(a1, b1)
{
	gamma = gamma1;
}
void AltalanosH::Szamol1() //Altalanos haromszog terulete
{
	ter = (sin(gamma * (3.14 / 180)) * a * b) / 2;
}
void AltalanosH::Szamol2() //Altalanos haromszog kerulete
{
	ker = sqrt((a * a) + (b * b)) + a + b; //??
}
