//barat
#include <iostream>
#include "Osztaly.h"
using namespace std;

int main()
{
	////////1.Feladat////////
	double a, b, c;
	cout << "Kerem a teglalap oldalait:" << endl
		 << "a oldal: ";
	cin >> a;
	cout << "b oldal: ";
	cin >> b;
	Teglalap t_lap(a, b);
	t_lap.Szamol1();
	t_lap.Szamol2();
	cout << "Teglalap kerulete: " << t_lap.GetEred1() << endl;
	cout << "Teglalap terulete: " << t_lap.GetEred2() << endl
		 << endl;

	cout << "Kerem a teglatest oldalait:" << endl
		 << "a oldal: ";
	cin >> a;
	cout << "b oldal: ";
	cin >> b;
	cout << "c oldal: ";
	cin >> c;
	Teglatest t_test(a, b, c);
	t_test.Szamol1();
	t_test.Szamol2();
	cout << "Teglatest felszine: " << t_test.GetEred1() << endl;
	cout << "Teglatest terfogata: " << t_test.GetEred2() << endl
		 << endl;

	Teglatest t_test2(1, 2, 3);
	t_test2.Szamol1();
	t_test2.Szamol2();
	cout << "Teglatest oldalai: 1,2,3" << endl
		 << "Teglatest felszine: " << t_test2.GetEred1() << endl
		 << "Teglatest terfogata: " << t_test2.GetEred2() << endl
		 << endl
		 << endl;

	////////2.Feladat////////
	int x;
	negyzet *pnegyzet;
	cout << "Negyzet oldala: ";
	cin >> x;
	pnegyzet = new negyzet(x);
	pnegyzet->Szamol1();
	pnegyzet->Szamol2();
	cout << "Negyzet kerulete: " << pnegyzet->GetEred1() << endl
		 << "Negyzet terulete: " << pnegyzet->GetEred2() << endl
		 << "Negyzet atlo: " << AtlotSzamol(*pnegyzet) << endl
		 << endl;

	kocka *pkocka;
	cout << "Kocka oldala: ";
	cin >> x;
	pkocka = new kocka(x);
	pkocka->Szamol1();
	pkocka->Szamol2();
	cout << "Kocka felszine:" << pkocka->GetEred1() << endl
		 << "Kocka terfogate: " << pkocka->GetEred2() << endl
		 << "Kocka lapatlo: " << AtlotSzamol(*pkocka) << endl
		 << "Kocka testatlo: " << TestatlotSzamol(*pkocka) << endl
		 << endl
		 << endl;

	////////3.Feladat////////
	//int x;
	int h;
	cout << "A kor es a gomb sugara: ";
	cin >> x;
	Kor kor_xy(x);
	kor_xy.Szamol1();
	kor_xy.Szamol2();
	cout << "Kor kerulete: " << kor_xy.GetEred1() << endl
		 << "Kor terulete: " << kor_xy.GetEred2() << endl;
	Gomb gomb_xy(x);
	gomb_xy.Szamol1();
	gomb_xy.Szamol2();
	cout << "Gomb kerulete: " << gomb_xy.GetEred1() << endl
		 << "Gomb terulete: " << gomb_xy.GetEred2() << endl
		 << endl;

	cout << "A henger sugara: ";
	cin >> x;
	cout << "Magassag: ";
	cin >> h;
	Henger henger_xy(x, h);
	henger_xy.Szamol1();
	henger_xy.Szamol2();
	cout << "Henger felszine: " << henger_xy.GetEred1() << endl
		 << "Henger terfogata: " << henger_xy.GetEred2() << endl
		 << endl
		 << endl;

	///////4.Feladat///////
	DerekSzH dsz_h;
	dsz_h.Szamol1();
	dsz_h.Szamol2();
	cout << "A derekszogu haromszog ket befogoja: 3,4" << endl
		 << "Derekszogu haromszog terulete: " << dsz_h.GetTer() << endl
		 << "Derekszogu haromszog kerulete: " << dsz_h.GetKer() << endl
		 << endl;

	AltalanosH alt_h(2, 4, 60);
	alt_h.Szamol1();
	alt_h.Szamol2();
	cout << "Az altalanos haromszog ket oldala: 2,4 es a kozbezart szoge: 60" << endl;
	cout << "Altalanos haromszog terulete: " << alt_h.GetTer() << endl
		 << "Altalanos haromszog kerulete: " << alt_h.GetKer();
	return 0;
}
