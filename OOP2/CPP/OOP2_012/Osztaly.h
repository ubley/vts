#ifndef OSZTALY_H_INCLUDED
#define OSZTALY_H_INCLUDED

///////1.Feladat////////
class Teglalap
{
protected:
	double a_oldal, b_oldal;
	double ered1, ered2;

public:
	Teglalap(double a1, double a2);
	void Szamol1(); // kerulet szamol az ered1 tagadatba
	void Szamol2(); // terulet szamol az ered2 tagadatba
	double GetEred1() { return ered1; };
	double GetEred2() { return ered2; };
};

class Teglatest : public Teglalap
{
protected:
	double c_oldal;

public:
	Teglatest(double a1, double b1, double c1);
	void Szamol1(); // felszint szamol az ered1 tagadatba
	void Szamol2(); // terfogatot szamol az ered2 tagadatba
};

///////2.Feladat////////
class negyzet
{
protected:
	double a_oldal;
	double ered1, ered2;

private:
	double atlo;

public:
	negyzet(double a1);
	void Szamol1(); //kerulet szamitas
	void Szamol2(); //terulet szamitas
	double GetEred1() { return ered1; }
	double GetEred2() { return ered2; }
	friend double AtlotSzamol(negyzet &n);
};

class kocka : public negyzet
{
private:
	double testatlo;

public:
	kocka(double a1);
	void Szamol1(); // felszin szamitasa
	void Szamol2(); // terfogat szamitasa
	friend double TestatlotSzamol(kocka &k);
};

///////3.Feladat////////
class Kor
{
protected:
	double r;
	double ered1, ered2;

public:
	Kor(double r1);
	void Szamol1(); // a kor kerulete
	void Szamol2(); // a kor terulete
	double GetEred1() { return ered1; }
	double GetEred2() { return ered2; }
};
class Gomb : public Kor
{
public:
	Gomb(double r1);
	void Szamol1(); // a gomb felszine
	void Szamol2(); // a gomb terfogata
};
class Henger : public Kor
{
protected:
	double h;

public:
	Henger(double r1, double h1);
	void Szamol1(); // a henger felszine
	void Szamol2(); // a henger terfogata
};

///////4.Feladat///////
class DerekSzH
{
protected:
	double a, b;
	double ter, ker;

public:
	DerekSzH(double a1, double b1)
	{
		a = a1;
		b = b1;
	}
	DerekSzH()
	{
		a = 3;
		b = 4;
	}
	void Szamol1();
	void Szamol2();
	double GetTer() { return ter; }
	double GetKer() { return ker; }
};
class AltalanosH : public DerekSzH
{
protected:
	double gamma;
	double c;

public:
	AltalanosH(double a1, double b1, double gamma1);
	void Szamol1();
	void Szamol2();
};

#endif // OSZTALY_H_INCLUDED
