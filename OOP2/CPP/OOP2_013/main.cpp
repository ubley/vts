#include <iostream>
using namespace std;

class Vektor
{
protected:
	int db;
	int *x;
	int osszeg;
	double atlag;

public:
	Vektor(int db1);
	~Vektor();
	void OsszegSzamol();
	void AtlagSzamol();
	int GetOsszeg() { return osszeg; }
	double GetAtlag() { return atlag; }
};
class Vektor2 : public Vektor
{
protected:
	int max, min, parosOsszeg;
	double paratlanAtlag;

public:
	Vektor2(int db1);
	void MaxKeres();
	void MinKeres();
	void ParosOsszegSzamol();
	void ParatlanAtlagSzamol();
	int GetMax() { return max; }
	int GetMin() { return min; }
	int GetParosOsszeg() { return parosOsszeg; }
	double GetParatlanAtlag() { return paratlanAtlag; }
};
//Vektor
Vektor::Vektor(int db1)
{
	db = db1;
	x = new int[db];
	osszeg = 0;
	for (int i = 0; i < db; i++)
	{
		cout << i << ". elem: ";
		cin >> x[i];
	}
};
Vektor::~Vektor()
{
	delete[] x;
	x = 0;
};
void Vektor::OsszegSzamol()
{
	for (int i = 0; i < db; i++)
	{
		osszeg += x[i];
	}
};
void Vektor::AtlagSzamol()
{
	double sum = osszeg; //int/int = int, double/int = double
	atlag = sum / db;
};
//Vektor2
Vektor2::Vektor2(int db1) : Vektor(db1)
{
	max = min = parosOsszeg = paratlanAtlag = 0;
};
void Vektor2::MaxKeres()
{
	max = x[0];
	for (int i = 1; i < db; i++)
	{
		if (max < x[i])
		{
			max = x[i];
		}
	}
};
void Vektor2::MinKeres()
{
	min = x[0];
	for (int i = 1; i < db; i++)
	{
		if (min > x[i])
		{
			min = x[i];
		}
	}
};
void Vektor2::ParosOsszegSzamol()
{
	for (int i = 0; i < db; i++)
	{
		if ((x[i] % 2) == 0)
		{
			parosOsszeg += x[i];
		}
	}
};
void Vektor2::ParatlanAtlagSzamol()
{
	double paratlanOsszeg = 0;
	int paratlandb = 0;
	for (int i = 0; i < db; i++)
	{
		if ((x[i] % 2) != 0)
		{
			paratlanOsszeg += x[i];
			paratlandb++;
		}
	}
	paratlanAtlag = paratlanOsszeg / paratlandb;
};

int main()
{
	int y;
	cout << "Vektor osztaly" << endl;
	cout << "Az adatok szama: ";
	cin >> y;
	Vektor vek(y);
	vek.OsszegSzamol();
	vek.AtlagSzamol();
	cout << "\nAz adatok osszege: " << vek.GetOsszeg() << endl
		 << "Az adatok atlaga: " << vek.GetAtlag();

	cout << "\n\nVektor2 osztaly" << endl;
	cout << "Az adatok szama: ";
	cin >> y;
	Vektor2 vek2(y);
	vek2.OsszegSzamol();
	vek2.AtlagSzamol();
	vek2.MinKeres();
	vek2.MaxKeres();
	vek2.ParosOsszegSzamol();
	vek2.ParatlanAtlagSzamol();
	cout << "\nAz adatok osszege: " << vek2.GetOsszeg() << endl
		 << "Az adatok atlaga: " << vek2.GetAtlag() << endl
		 << "A legkisebb adat: " << vek2.GetMin() << endl
		 << "A legnagyobb adat: " << vek2.GetMax() << endl
		 << "Paros adatok osszege: " << vek2.GetParosOsszeg() << endl
		 << "Paratlan adatok atlaga: " << vek2.GetParatlanAtlag();
	return 0;
};
