#include "Osztaly.h"
#include <math.h>
#include <iostream>

using namespace std;

	DHaromszog::DHaromszog(double a1, double b1)
	{
		a=a1;
		b=b1;
	}

	void DHaromszog::Szamol1()
	{
		c=sqrt(a*a + b*b);
	}

	void DHaromszog::Szamol2()
	{
		eredmeny1 = a+b+c;
		eredmeny2 = (a*b)/2;
	}

	void DHaromszog::Kiir()
	{

		cout << "Atfogo: " << c << endl <<
		 		"Kerulet: " << eredmeny1 << endl <<
				"Terulet: " << eredmeny2 << endl;
	}

	double kor_sugara(DHaromszog &dh)
	{
		return dh.c;
	}
