#ifndef OSZTALY_H_INCLUDED
#define OSZTALY_H_INCLUDED

class DHaromszog
{
  private:
	double a,b;
	double c;
	double eredmeny1,eredmeny2;
  public:
	DHaromszog(double a1, double b1);
	DHaromszog() {a = 3; b = 4; }
	double GetA() { return a; }
	double GetB() { return b; }
	void Szamol1();  // átfogót és kerületet számol
	void Szamol2();  // kerület && terület
	void Kiir();

	friend double kor_sugara(DHaromszog &dh);
};

#endif // OSZTALY_H_INCLUDED
