#include <iostream>
#include <string>
using namespace std;

class Alkalmazott
{
	string nev;
	int kereset;
	double ledolgozott_orak;

public:
	Alkalmazott()
	{
		nev = "N/A";
		kereset = 0;
	}
	Alkalmazott(string nev, double ledolgozott_orak)
	{
		this->nev = nev;
		this->ledolgozott_orak = ledolgozott_orak;
	}
	virtual void fizetes() = 0;
	int GetKereset() { return kereset; }
	void SetKereset(int kereset)
	{
		this->kereset = kereset;
	}
	double GetOrak() { return ledolgozott_orak; }
	string GetNev() { return nev; }
};

class Eladasban_dolgozo : public Alkalmazott
{
	int eladott_aru;

public:
	Eladasban_dolgozo() : Alkalmazott()
	{
		eladott_aru = 0;
	}
	Eladasban_dolgozo(string nev, int ledolgozott_orak, int eladott_aru) : Alkalmazott(nev, ledolgozott_orak)
	{
		this->eladott_aru = eladott_aru;
	}
	void fizetes()
	{
		SetKereset(GetOrak() * eladott_aru);
	}
	int GetEa() { return eladott_aru; }
};

class Termelesben_dolgozo : public Alkalmazott
{
	int aru_termeles;

public:
	Termelesben_dolgozo() : Alkalmazott()
	{
		aru_termeles = 0;
	}
	Termelesben_dolgozo(string nev, int ledolgozott_orak, int aru_termeles) : Alkalmazott(nev, ledolgozott_orak)
	{
		this->aru_termeles = aru_termeles;
	};
	void fizetes()
	{
		SetKereset(GetOrak() * aru_termeles);
	}
	int GetAt() { return aru_termeles; }
};

class Data_Td
{
	Termelesben_dolgozo tm_d[20];
	int db;

public:
	Data_Td()
	{
		string str;
		int lo, at;
		do
		{
			cout << "Bevinni kivant dolgozok: ";
			cin >> db;
			if (db > 20)
				cout << "HIBA, max 20 lehet" << endl;
		} while (db > 20);
		for (int i = 0; i < db; i++)
		{
			cout << i + 1 << ".";
			cout << "\nKerem a munkas nevet: ";
			cin >> str;
			cout << "Kerem a ledolgozott ora szamat: ";
			cin >> lo;
			cout << "Kerem a kitermelt aru mennyiseget: ";
			cin >> at;
			tm_d[i] = {str, lo, at};
			tm_d[i].fizetes();
		}
	}
	void kiir()
	{
		for (int i = 0; i < db; i++)
		{
			cout << i + 1 << ". munkas:" << endl
				 << "nev: " << tm_d[i].GetNev() << endl
				 << "ora: " << tm_d[i].GetOrak() << endl
				 << "aru: " << tm_d[i].GetAt() << endl
				 << "fizetes: " << tm_d[i].GetKereset() << "eu" << endl;
		}
	}
	void RendezNev()
	{
		Termelesben_dolgozo otemp;
		for (int i = 0; i < (db - 1); i++)
		{
			for (int j = 0; j < (db - i - 1); j++)
			{
				if (tm_d[j].GetNev() > tm_d[j + 1].GetNev())
				{
					otemp = tm_d[j];
					tm_d[j] = tm_d[j + 1];
					tm_d[j + 1] = otemp;
				}
			}
		}
	}
	void RendezJell()
	{
		Termelesben_dolgozo otemp;
		for (int i = 0; i < (db - 1); i++)
		{
			for (int j = 0; j < (db - i - 1); j++)
			{
				if (tm_d[j].GetAt() > tm_d[j + 1].GetAt())
				{
					otemp = tm_d[j];
					tm_d[j] = tm_d[j + 1];
					tm_d[j + 1] = otemp;
				}
			}
		}
	}
};

int main()
{
	Data_Td obj1;
	cout << "\nRENDEZES NEV SZERINT" << endl;
	obj1.RendezNev();
	obj1.kiir();
	cout << "\nRENDEZES JELLEMZO SZERINT" << endl;
	obj1.RendezJell();
	obj1.kiir();
};
