#include <iostream>
#include "Osztaly.h"

using namespace std;

int main()
{

    Kor k1;
    k1.set_sugar(10);
    cout << "Kor sugara= "   << k1.get_sugar() << endl;
    cout << "Kor terulete= " << k1.kor_terulete() << endl;
    cout << "Kor kerulete= " << k1.kor_kerulete() << endl << endl;


    Gomb g1;
    g1.set_sugar(10);
    cout << "Gomb sugara= " << g1.get_sugar() << endl;
    cout << "Gomb terfogata= " << g1.gomb_terfogata() << endl;
    cout << "Gomb felszine= " << g1.gomb_felszine() << endl << endl;

    Henger h1;
    h1.init(10,5);
    cout << "Henger sugara= " << h1.get_sugar() << endl;
    cout << "Henger terfogata= " << h1.henger_terfogata() << endl;
    cout << "Hemger palast terulete= " << h1.henger_palast_terulete() << endl << endl;

    return 0;
}
