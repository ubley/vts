#ifndef OSZTALY_H_INCLUDED
#define OSZTALY_H_INCLUDED

class Kor
{
protected:
    int r;
public:
    void set_sugar(int r0){r=r0;}
    int get_sugar(){return r;}
    float kor_terulete();
    float kor_kerulete();
};

class Gomb : public Kor
{
public:
    float gomb_terfogata();
    float gomb_felszine();
};

class Henger : public Kor
{
    int h;
public:
    void init(int, int);
    float henger_terfogata();
    float henger_palast_terulete();
};

#endif // OSZTALY_H_INCLUDED
