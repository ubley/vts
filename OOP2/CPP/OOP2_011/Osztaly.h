#ifndef OSZTALY_H_INCLUDED
#define OSZTALY_H_INCLUDED
#include <string>
using namespace std;

class Szemely
{
public:
	string nev;
	int ev;

	void init (string nev0, int ev0);
	Szemely();
	~Szemely();
};

class Csoport
{
	Szemely *cs[20];
	int letszam;
public:
	Csoport();
	~Csoport();
	void nev_rendez();
	void ev_rendez();
	void kiir();
};

#endif // OSZTALY_H_INCLUDED
