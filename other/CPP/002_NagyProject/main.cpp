#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

struct adat
{
	string word;
	int wc = 1;
	adat *Link;
}*start, *utolso;

void valasztas();
bool ellenorzes (string szo1);
void bubblesort ();
void kiiras ();
void szotorlese ();
void valogatasfajlba ();

int main()
{
	adat *aktualis;

	aktualis = new adat;
	start = utolso = aktualis;
	aktualis->Link = NULL;

	string word1 = "";
	bool begin = false;

	ifstream file1("data.txt");
	while (!file1.eof())
	{
		file1 >> word1;
		word1.at(0) = tolower(word1.at(0));

		if (!(ellenorzes(word1)))
		{
			if (begin)
				aktualis = new adat;
			begin = true;
			aktualis->word = word1;

			utolso->Link = aktualis;
			aktualis->Link = NULL;
			utolso = aktualis;
		}
	}
	valasztas();

	return 0;
}

void valasztas()
{
	int i;
	cout << endl << "Irjon be egy szamot: " << endl << "1: kiiras" << endl <<
	"2: bubblesort" << endl << "3: szo torlese" << endl << "4: valogatas" << endl << "5: exit" << endl;
	cin >> i;
	switch (i)
	{
	case 1: kiiras(); break;
	case 2: bubblesort(); break;
	case 3: szotorlese(); break;
	case 4: valogatasfajlba(); break;
	case 5: exit(1);
	default: valasztas(); break;
	}
}

void kiiras()
{
	adat *akt;
	akt = start;
	cout << setw(20) << "A lista adatai" << endl;
	for (int i = 1; (akt != NULL); i++)
	{
		cout << i << ". csomopont:" << endl;
		cout << setw(15) << "Szo: " << akt->word << endl;
		cout << setw(15) << "DB: " << akt->wc << endl;
		akt = akt->Link;
	}
	valasztas();
}


bool ellenorzes(string szo1)
{
	adat *akt;
	akt = start;
	while (akt != NULL)
	{
		if (akt->word == szo1)
		{
			akt->wc++;
			return true;
		}
		akt = akt->Link;
	}
	return false;
}

void bubblesort()
{
	adat *elozo, *kovetkezo, *temp;
	temp = new adat;

	for (elozo = start; (elozo != utolso); elozo = elozo->Link)
	{
		for (kovetkezo = elozo->Link; (kovetkezo != NULL); kovetkezo = kovetkezo->Link)
		{
			if ( elozo->wc > kovetkezo->wc )
			{
				temp->wc = elozo->wc; 		temp->word = elozo->word;
				elozo->wc = kovetkezo->wc; 	elozo->word = kovetkezo->word;
				kovetkezo->wc = temp->wc; 	kovetkezo->word = temp->word;
			}
		}
	}
	valasztas();
}

void szotorlese()
{
	adat *akt, *elozo;
	elozo = start;
	akt = start->Link;
	string szo1;
	cout << "\nVigye be a torolni kivant szot: ";
	cin >> szo1;

	if (start->word == szo1)
	{
		*start = *start->Link;
	}
	else
	{
		while (akt != NULL)
		{
			if (akt->word == szo1)
			{
				elozo->Link = akt->Link;
				delete akt;
				break;
			}
			akt = akt->Link;
			elozo = elozo->Link;
		}
	}
	valasztas();
}

void valogatasfajlba()
{
	adat *akt;
	int x, y, i = 1;
	akt = start;
	cout << "\nAdja meg az also hatart: ";
	cin >> x;
	cout << "\nAdja meg a felso hatart: ";
	cin >> y;
	ofstream file2("savedata.txt");
	while (akt != NULL)
	{
		if (akt->wc >= x && akt->wc <= y)
		{
			file2 << i << ". csomopont" << endl;
			file2 << setw(15) << "Szo: " << akt->word << endl;
			file2 << setw(15) << "DB: " << akt->wc << endl;
			i++;
		}
		akt = akt->Link;
	}
	file2.close();
	valasztas();
}
