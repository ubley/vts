#include <iostream>
#include <fstream>
using namespace std;

struct adatok
{
    int ar;
    int db;
    string nev;
};

int main()
{
    int tm = 0;
    adatok t[280];
    fstream file1("adat.txt");
    while (!file1.eof())
    {
        file1 >> t[tm].nev >> t[tm].ar >> t[tm].db;
        tm++;
    }

    for (int i = 0; i < tm - 1; i++)
    {
        int min = t[i].ar;
        int loc = i;
        for (int j = i + 1; j < tm; j++)
        {
            if (min > t[j].ar)
            {
                min = t[j].ar;
                loc = j;
            }
        }

        int temp = t[i].ar;   string stemp = t[i].nev;  int dbtemp = t[i].db;
        t[i].ar = t[loc].ar;  t[i].nev = t[loc].nev;    t[i].db = t[loc].db;
        t[loc].ar = temp;     t[loc].nev = stemp;       t[loc].db = dbtemp;
    }

    for (int i = 0; i < tm; i++)
        cout << "\nNev: " << t[i].nev << " Ar: " << t[i].ar << " DB: " << t[i].db;

    return 0;

}
