import java.io.File;
import java.io.FileWriter;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {

	String nev;
	int db;
	int ar;

	public static void main(String[] args) {
		int tm = 0;
		Main t[] = new Main[280];
		File file1 = new File("adat.txt");
		try {
			Scanner finput = new Scanner(file1);
			while (finput.hasNext()) {
				t[tm] = new Main();
				t[tm].nev = finput.next();
				t[tm].ar = finput.nextInt();
				t[tm].db = finput.nextInt();
				tm++;
			}
			finput.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		for (int i = 0; i < tm - 1; i++) {
			int min = t[i].ar;
			int loc = i;
			for (int j = i + 1; j < tm; j++) {
				if (min > t[j].ar) {
					min = t[j].ar;
					loc = j;
				}
			}
			int temp = t[i].ar;   String stemp = t[i].nev;  int dbtemp = t[i].db;
			t[i].ar = t[loc].ar;  t[i].nev = t[loc].nev;    t[i].db = t[loc].db;
			t[loc].ar = temp;     t[loc].nev = stemp;       t[loc].db = dbtemp;
		}

		try {
			FileWriter foutput = new FileWriter("save.txt");
			for (int i = 0; i < tm; i++)
				foutput.write("Nev: " + t[i].nev + "\tAr: " + t[i].ar + "\tdb: " + t[i].db +"\n");
			foutput.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}