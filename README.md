# -VTS-

Ajánlom használni külön mappa/file letöltéséhez a következőket:
- [Chrome kiegészítő](https://chrome.google.com/webstore/detail/gitzip-for-github/ffabmkklhbepgcgfonabamgnfafbdlkn)
- [Firefox kiegészítő](https://addons.mozilla.org/en-US/firefox/addon/gitzip/)
- [GitZip](http://kinolien.github.io/gitzip/)
- [DownGit](https://minhaskamal.github.io/DownGit/#/home)
